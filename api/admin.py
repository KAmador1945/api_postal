from django.contrib import admin
from .models import Departamentos, Municipio, Vecindarios, CodigoPostal

# Register your models here.
class ObjectAdmin(admin.ModelAdmin):
    ordering = ['-order']

admin.site.register(Departamentos)
admin.site.register(Municipio)
admin.site.register(Vecindarios)
admin.site.register(CodigoPostal)