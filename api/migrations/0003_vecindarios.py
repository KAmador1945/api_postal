# Generated by Django 3.1.5 on 2021-01-16 06:20

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_municipio'),
    ]

    operations = [
        migrations.CreateModel(
            name='Vecindarios',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre_vecd', models.CharField(blank=True, max_length=120, null=True)),
                ('munip', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='id_municipio', to='api.municipio')),
            ],
        ),
    ]
