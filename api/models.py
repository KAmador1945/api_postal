from django.db import models

# Create your models here.
class CodigoPostal(models.Model):

    # define fields
    cod_num = models.CharField(max_length=5, null=True, blank=True)

    def __str__(self):
        return str(self.cod_num)


class Departamentos(models.Model):

    # define fields
    num_cod = models.ForeignKey(CodigoPostal, related_name='num_cod', on_delete=models.CASCADE, null=True)
    nombre = models.CharField(max_length=120, null=True, blank=True)
    status = models.BooleanField(default=True)

    def __str__(self):
        return str(self.nombre)

class Municipio(models.Model):

    # define fields
    depart = models.ForeignKey(Departamentos, related_name='id_departamentos', on_delete=models.CASCADE)
    nombre_mn = models.CharField(max_length=120, null=True, blank=True)
    postal = models.OneToOneField(CodigoPostal, related_name='postals_code', null=True, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.nombre_mn)

class Vecindarios(models.Model):

    # define fields
    munip = models.ForeignKey(Municipio, related_name='id_municipio', on_delete=models.CASCADE)
    nombre_vecd = models.CharField(max_length=120, null=True, blank=True)
    postal = models.ManyToManyField(CodigoPostal, related_name='postals')

    def __str__(self):
        return str(self.nombre_vecd)
