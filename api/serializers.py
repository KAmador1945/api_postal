from rest_framework import serializers
from .models import Departamentos, Vecindarios, CodigoPostal, Municipio


# Departamentos Serializers
class PostalCodesSerializers(serializers.ModelSerializer):
    class Meta:
        model = CodigoPostal
        fields = (
            'id',
            'cod_num'
        )

class MunicipioSerializers(serializers.ModelSerializer):
    class Meta:
        model = Municipio
        fields = (
            'id',
            'nombre_mn'
        )

class VecindariosSerializers(serializers.ModelSerializer):

    class Meta:
        model = Vecindarios
        fields = (
            'id',
            'nombre_vecd',
        )

class MiniDepsSerializers(serializers.ModelSerializer):
    class Meta:
        model = Departamentos
        fields = (
            'id',
            'nombre',
        )

class DepartamentosSerializers(serializers.ModelSerializer):
    # make query

    class Meta:
        model = Departamentos
        fields = (
            'id',
            'nombre',
            'status',
        )
