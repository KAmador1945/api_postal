from django.urls import path, include
from rest_framework import routers

router = routers.DefaultRouter()
# router.register('info', GetAllInfo)
# router.register('deps', DepartamentosView)

urlpatterns = [
    path('', include(router.urls)),
    # path('deps/<str:postal>/', GetAllInfo.as_view()),
]