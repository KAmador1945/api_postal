from django.core import serializers
from .models import Departamentos, CodigoPostal, Municipio, Vecindarios
from django.http import HttpResponse, JsonResponse

#
def GetInfo(request, postal):
    if request.method == "GET":
        if postal is not None:
            # find postal code
            try:
                postal_code = CodigoPostal.objects.all().get(cod_num=postal)
                #get postal code id and dep
                dep = Departamentos.objects.all().get(num_cod=postal_code.id)
                # get muni
                mun = Municipio.objects.all().filter(depart_id=dep.id)
                if mun is not None:
                    #qt
                    for mn in mun:
                        qt = Vecindarios.objects.all().filter(munip_id=mn.id)
                        if qt:
                            try:
                                data = serializers.serialize('python', qt)
                                actual = [d['fields'] for d in data]
                                return JsonResponse(actual, safe=False)
                            except Vecindarios.DoesNotExist:
                                msg = {
                                    'message':'Does not exist'
                                }
                                return JsonResponse(msg, safe=False)
                        else:
                            try:
                                data = serializers.serialize('python', mun)
                                actual = [d['fields'] for d in data]
                                return JsonResponse(actual, safe=False)
                            except mn.DoesNotExist:
                                msg = {
                                    'message':'Does not exist'
                                }
                                return JsonResponse(msg, safe=False)
                else:
                    msg = {
                        'message':'Opps does not exist nothig'
                    }
                    return JsonResponse(msg)
            except CodigoPostal.DoesNotExist:
                msg = {
                    'message':'postal code does not exist'
                }
                return JsonResponse(msg)
        else:
            msg = {
                'message':'does not exist nothing'
            }
            return JsonResponse(msg)

    else:
        msg = {
            'message':'Try again'
        }

        return JsonResponse(msg, safe=False)
